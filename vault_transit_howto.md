####
# нужно отправить POST с телом json:
# {"role_id": "924feb3b-0f71-a917-3d8d-c52cd91bf225" ,"secret_id":"2d48c5a6-8f64-680c-21f2-c139e19bef86"}
# на адрес http://vault.dev.k8s.local/v1/auth/approle/login
# вернется json:
# {
#     "request_id": "2b054823-1d76-5a18-6add-84b589fe0e6e",
#     "lease_id": "",
#     "renewable": false,
#     "lease_duration": 0,
#     "data": null,
#     "wrap_info": null,
#     "warnings": null,
#     "auth": {
#         "client_token": "hvs.CAESILZiSi9Fzo7l_GpSTQu8l0rzI3YC88ppSsyPc6TXqFbxGh4KHGh2cy5pajJ1V0R3MlBXVkU4UXFEZG5pRGVYeUg",
#         "accessor": "ZfjPTwPX1Ds5EugLXlbMgAVV",
#         "policies": [
#             "default",
#             "example-project"
#         ],
#         "token_policies": [
#             "default",
#             "example-project"
#         ],
#         "metadata": {
#             "role_name": "example-project"
#         },
#         "lease_duration": 2764800,
#         "renewable": true,
#         "entity_id": "f571773f-38dc-9d18-3387-4155bb88bfbc",
#         "token_type": "service",
#         "orphan": true,
#         "mfa_requirement": null,
#         "num_uses": 0
#     }
# }
# отсюда нужен - 
#  "client_token": "hvs.CAESILZiSi9Fzo7l_GpSTQu8l0rzI3YC88ppSsyPc6TXqFbxGh4KHGh2cy5pajJ1V0R3MlBXVkU4UXFEZG5pRGVYeUg",

# после в header указываем:
# X-Vault-Token: hvs.CAESILZiSi9Fzo7l_GpSTQu8l0rzI3YC88ppSsyPc6TXqFbxGh4KHGh2cy5pajJ1V0R3MlBXVkU4UXFEZG5pRGVYeUg
# в теле отправляем json:
# {
#     "plaintext": "0J/RgNC40LLQtdGC" #BASE64 текст
# }
# и посылаем POST на url 
# http://vault.dev.k8s.local/v1/transit/encrypt/example-project
# он вернет json:
# {
#     "request_id": "b8797976-5c10-1a0d-bd97-ff9b53b9a977",
#     "lease_id": "",
#     "renewable": false,
#     "lease_duration": 0,
#     "data": {
#         "ciphertext": "vault:v1:vjhnqYJLYfXOnNF73ToeYsBzWoRif20EaniJiDa5sQ==",
#         "key_version": 1
#     },
#     "wrap_info": null,
#     "warnings": null,
#     "auth": null
# }
# где сам шифрованный текст содержится тут: 
# "ciphertext": "vault:v1:vjhnqYJLYfXOnNF73ToeYsBzWoRif20EaniJiDa5sQ==",
# для расшифровки нужно отправить;

# в header указываем:
# X-Vault-Token: hvs.CAESILZiSi9Fzo7l_GpSTQu8l0rzI3YC88ppSsyPc6TXqFbxGh4KHGh2cy5pajJ1V0R3MlBXVkU4UXFEZG5pRGVYeUg
# в теле отправляем json:
# {
#     "ciphertext": "vault:v1:vjhnqYJLYfXOnNF73ToeYsBzWoRif20EaniJiDa5sQ=="
# }
# и посылаем POST на url 
# http://vault.dev.k8s.local/v1/transit/decrypt/example-project
# он вернет:
# {
#     "request_id": "10c8113f-b387-98b7-585d-69733512e8b1",
#     "lease_id": "",
#     "renewable": false,
#     "lease_duration": 0,
#     "data": {
#         "plaintext": "0J/RgNC40LLQtdGC"
#     },
#     "wrap_info": null,
#     "warnings": null,
#     "auth": null
# }
# где текст содаржится в 
# "plaintext": "0J/RgNC40LLQtdGC"
#  в формате BASE64